


function addOrder(){
    var note=document.getElementById("note").value;
    var qty=document.getElementById("qty").value;
    var dish=document.getElementById("selectedDish").innerHTML;
    var table=document.getElementById("orderTable");
    table.innerHTML=table.innerHTML+"<tr><td>"+dish+"</td><td>"+qty+"</td><td>"+note+"</td></tr>";
}



async function submitOrder(){
    const url = 'http://localhost:5000/manage/orders/';
    var note;
    var dish;
    var qty;
    var table=document.getElementById("orderTable");
    var name= document.getElementById("name").value;
    var orderArray=[];
    for (i = 1; i < table.rows.length; i++) {
        var objCells = table.rows.item(i).cells;
        dish=objCells.item(0).innerHTML;
        qty=objCells.item(1).innerHTML;
        note=objCells.item(2).innerHTML;
        orderArray.push({dish,qty,note,name});
    }

    for (var order of orderArray){
        data={
            "dish_name": order.dish,
            "amount_ordered": order.qty,
            "order_time": new Date(),
            "note": order.note,
            "customer_name": name
        };
        console.log(data);
        try {
          const response = await fetch(url, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            }
          });
          const json = await response.json();
          console.log('Success:', JSON.stringify(json));
        } catch (error) {
              console.error('Error:', error);
        }
    }
}
