async function addIngredient(){
    var ingredient_name= document.getElementById("ingredient_name").value;
    var unit=document.getElementById("unit").value;
    data={
        "ingredient_name": ingredient_name,
        "amount_present": 0,
        "unit": unit,
        "amount_optimum": 0,
        "amount_used": 0,
        "minimum_Threshold": 0,
        "amount_ordered": 0,
        "last_ordered": new Date(0),
        "category": "none"
    }
    const url = 'http://localhost:5000/manage/ingredients/';
    try {
        const response = await fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          }
        });
        const json = await response.json();
        console.log('Success:', JSON.stringify(json));
      } catch (error) {
            console.error('Error:', error);
      }

}