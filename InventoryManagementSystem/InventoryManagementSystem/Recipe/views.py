from InventoryManagementSystem.Recipe.models import Recipe
from rest_framework import viewsets
from InventoryManagementSystem.Recipe.serializers import RecipeSerializer

from django.http import HttpResponse
from django.template import loader

class RecipeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Recipe.objects.all().order_by('dish_name')
    serializer_class = RecipeSerializer



def index(request):
    recipe_list = Recipe.objects.all().order_by('dish_name')
#    print(ingredients_list[0].ingredient_name)
    template = loader.get_template('Recipe/index.html')
    context = {
        'recipe_list': recipe_list,
    }
    return HttpResponse(template.render(context, request))















