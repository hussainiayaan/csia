
from InventoryManagementSystem.Recipe.models import Recipe

from rest_framework import serializers


class RecipeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Recipe
        fields = ('dish_name',
                  'precedure',
                  "ingredient_name",
                  'ingredient_amount')
