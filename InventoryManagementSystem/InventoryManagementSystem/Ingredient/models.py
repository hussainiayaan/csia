from django.db import models

class Ingredient(models.Model):
    ingredient_name = models.TextField(default='error')
    amount_present = models.FloatField()
    unit = models.TextField()
    amount_optimum = models.FloatField()
    amount_used = models.FloatField()
    minimum_Threshold = models.FloatField()
    amount_ordered = models.FloatField()
    last_ordered = models.DateTimeField()
    category= models.TextField()
    
