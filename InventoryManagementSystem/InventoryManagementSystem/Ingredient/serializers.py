
from InventoryManagementSystem.Ingredient.models import Ingredient

from rest_framework import serializers


class  IngredientsSerializer(serializers.HyperlinkedModelSerializer,):
    class Meta:
        model = Ingredient
        fields = ('ingredient_name', 'amount_present', 'unit',
                  'amount_optimum','amount_used', 'minimum_Threshold',
                  'amount_ordered', 'last_ordered', 'category')

