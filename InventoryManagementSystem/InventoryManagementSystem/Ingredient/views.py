from InventoryManagementSystem.Ingredient.models import Ingredient
from rest_framework import viewsets
from rest_framework import generics
from InventoryManagementSystem.Ingredient.serializers import IngredientsSerializer

from django.http import HttpResponse
from django.template import loader

class IngredientsViewSet(generics.UpdateAPIView):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Ingredient.objects.all().order_by('ingredient_name')
    serializer_class = IngredientsSerializer
    
    

def index(request):
    ingredients_list = Ingredient.objects.all().order_by('ingredient_name')
#    print(ingredients_list[0].ingredient_name)
    template = loader.get_template('Ingredient/index.html')
    context = {
        'ingredients_list': ingredients_list,
    }
    return HttpResponse(template.render(context, request))

def newIngredient(request):
    template = loader.get_template('Ingredient/newIngredient.html')
    return HttpResponse(template.render({}, request))













