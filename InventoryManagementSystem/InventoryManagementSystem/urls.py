"""InventoryManager URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from InventoryManagementSystem.Ingredient import views as ingredientViews
from InventoryManagementSystem.Orders import views as ordersViews
from InventoryManagementSystem.Recipe import views as recipeViews

router = routers.DefaultRouter()
router.register(r'ingredients', ingredientViews.IngredientsViewSet)
router.register(r'orders', ordersViews.OrdersViewSet)
router.register(r'recipes', recipeViews.RecipeViewSet)
# router.register(r'delete_orders', ordersViews.DeleteOrder)


urlpatterns = [
    path('ingregients/', ingredientViews.index, name='index'),
    path('recipes/', recipeViews.index, name='index'),
    path('orders/', ordersViews.index, name='index'),
    path('delete_orders/', ordersViews.deleteOrder, name='index'),
    path('new_orders/', ordersViews.newOrders, name='newOrders'),
        path('new_ingredient/', ingredientViews.newIngredient, name='newIngredient'),

    path('manage/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/',
         include('rest_framework.urls',
                 namespace='rest_framework'))
]
