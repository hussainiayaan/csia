
from django.db import models

class Orders(models.Model):
    dish_name = models.TextField(default='error')
    amount_ordered = models.FloatField()
    order_time = models.DateTimeField()
    note = models.TextField(default='error')
    customer_name = models.TextField(default='error')

