
from InventoryManagementSystem.Orders.models import Orders

from rest_framework import serializers


class OrdersSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Orders
        fields = ('dish_name', 'amount_ordered', 'order_time', 'note','customer_name')

