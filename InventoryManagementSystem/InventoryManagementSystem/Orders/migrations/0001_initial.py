# Generated by Django 2.1.2 on 2019-07-27 10:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Orders',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dish_name', models.TextField(default='error')),
                ('amount_ordered', models.FloatField()),
                ('order_time', models.DateTimeField()),
            ],
        ),
    ]
